## GIT BIT UNITY
---
### Pre-Colab items that need to be taken care of.
1. Everyone needs to be added to the quoteunquote workspace
    * This is done in BitBucket via the Workspace settings
    * I took care of this by adding Ryan, Sean, and I to the Default Group "Developers"
        - This group is added to all quoteunquote repositories by default.
        - This can be altered at a later time. And I am more than happy to assit with walking through the various Admin tasks.
2. Everyone needs to double check their SourceTree settings and make sure that their BitBucket account is actually signed in.
    * I found that mine was not and I am guessing that neither was Sean's when we trying to get everything rolling on our call.
    * This can be checked in the Options menu.
        - Tools > Options > Authentication
    * If you are not logged in
        - Click on Add
        - Then click RefreshOAuth Token.
3. Everyone will need to generate an App password in BitBucket. This is required for SourceTree to communicate with BitBucket repositories.
    * With BitBucket open, click on your initials in the bottom left hand corner and select Personal Settings.
    * Under Access Management click on App Passwords.
    * Then Create app password.
        - Any name will do.
    * Add read and write permissions to anything you think you might need.
    * CLick Create
4. GIT LFS extension should be installed.
    * SourceTree and BitBucket started yelling at me when I attempted to stage the initial commit and push.
    * I have included a well rounded .gitattribute file that takes care of Large File tracking. So all you guys have to do is install GIT LFS. No additional configurations needed.
    * Just make sure that the .gitattribute and .gitignore files stay in the project's root directory.

I believe that is it.

For now.